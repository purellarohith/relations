import { JobContract } from '@ioc:Rocketseat/Bull';
import execa from 'execa';

/*
|--------------------------------------------------------------------------
| Job setup
|--------------------------------------------------------------------------
|
| This is the basic setup for creating a job, but you can override
| some settings.
|
| You can get more details by looking at the bullmq documentation.
| https://docs.bullmq.io/
*/

export default class Webscrape implements JobContract {
    public key = 'Webscrape';

    public async handle(job) {
        const { data } = job;
        const { exitCode, stdout } = await execa.command(`py ${data.file}`);
        console.log("exitCode", exitCode, stdout);
        // Do somethign with you job data
    }
}
