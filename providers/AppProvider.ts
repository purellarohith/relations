import { ApplicationContract } from '@ioc:Adonis/Core/Application'

export default class AppProvider {
    constructor(protected app: ApplicationContract) { }

    public register() {
        // Register your own bindings
    }

    public async boot() {
        // IoC container is ready
        const { default: Bull } = await import('@ioc:Rocketseat/Bull')
        Bull.add('Webscrape', { file: 'scrape.py' }, {
            repeat: {
                // every: ((24 * 86400) * 1000)
                every: 15 * 1000
            }
        });
    }

    public async ready() {
        // App is ready
    }

    public async shutdown() {
        // Cleanup, since app is going down
    }
}
